import jsPDF from 'jspdf';

function exportAsPDF() {
  const frame = document.getElementById('frame');

  // Create a new jsPDF instance
  const doc = new jsPDF();

  // Get the dimensions of the frame
  const width = frame.offsetWidth;
  const height = frame.offsetHeight;

  // Create an image from the frame contents
  const imgData = frame.toDataURL('image/png');

  // Add the image to the PDF document
  doc.addImage(imgData, 'PNG', 0, 0, width, height);

  // Save the PDF document
  doc.save('frame.pdf');
}