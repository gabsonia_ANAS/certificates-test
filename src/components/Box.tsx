import type { CSSProperties, FC, ReactNode } from "react";
import { useDrag } from "react-dnd";

import { ItemTypes } from "../types/ItemTypes";
import "../styles/Box.css";

export interface BoxProps {
  left: number;
  top: number;
  children?: ReactNode;
}

export const Box: FC<BoxProps> = ({ left, top, children }) => {
  const [, drag] = useDrag(
    () => ({
      type: ItemTypes.BOX,
      item: { left, top },
    }),
    [left, top]
  );

  return (
    <div className="box" ref={drag} style={{ left, top }}>
      {children}
    </div>
  );
};
