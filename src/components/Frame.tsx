import React from "react";
import "../styles/Frame.css";
import { useDrop } from "react-dnd";
import { ItemTypes } from "../types/ItemTypes";
import { DragItem } from "../types/DragITem";
import type { XYCoord } from "react-dnd";
import { Box } from "./Box";

type FrameProps = {
  frameWidth: number;
  frameHeight: number;
  backgroundImage: string | null;
  onItemDrop: (item: DragItem, left: number, right: number) => void;
  box: {
    left: number;
    top: number;
  };
};

function Frame(props: FrameProps) {
  const frameStyle = {
    width: `${props.frameWidth}px`,
    height: `${props.frameHeight}px`,
    backgroundImage: props.backgroundImage
      ? `url(${props.backgroundImage})`
      : ``,
    backgroundSize: "100% 100%",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center",
  };

  const [, drop] = useDrop(
    () => ({
      accept: ItemTypes.BOX,
      drop(item: DragItem, monitor) {
        const delta = monitor.getDifferenceFromInitialOffset() as XYCoord;
        const left = Math.round(item.left + delta.x);
        const top = Math.round(item.top + delta.y);

        props.onItemDrop(item, left, top);

        return undefined;
      },
    }),
    [props.box]
  );

  return (
    <div id="abdo" className="frame" style={frameStyle} ref={drop}>
      <Box left={props.box.left} top={props.box.top}>
        {`test text`}
      </Box>
    </div>
  );
}

export default Frame;
