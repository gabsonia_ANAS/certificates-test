import React, { useState } from "react";
import "../styles/Sidebar.css";
import { Box } from "./Box";

type SidebarProps = {
  frameWidth: number;
  frameHeight: number;
  box: {
    left: number;
    top: number;
  };
  onFrameWidthChange: (width: number) => void;
  onFrameHeightChange: (height: number) => void;
  onBackgroundImageChange: (image: string) => void;
  onSaveClick: () => void;
  onJsonInsert: (json: string) => void;
  onExport: () => void;
};

const Sidebar = (props: SidebarProps) => {
  const [jsonText, setJsonText] = useState("");

  const handleJsonTextChange = (
    event: React.ChangeEvent<HTMLTextAreaElement>
  ) => {
    setJsonText(event.target.value);
    props.onJsonInsert(event.target.value);
  };

  const handleFrameWidthChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    props.onFrameWidthChange(Number(event.target.value));
  };

  const handleFrameHeightChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    props.onFrameHeightChange(Number(event.target.value));
  };

  const handleBackgroundImageChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const file = event.target.files?.[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = (e) => {
        const dataUrl = e.target?.result;
        if (dataUrl) {
          localStorage.setItem("bgImage", dataUrl.toString());
          props.onBackgroundImageChange(dataUrl.toString());
        }
      };
      reader.readAsDataURL(file);
    }
  };

  return (
    <div className="sidebar">
      <h2>Certificate Settings</h2>
      <label htmlFor="frameWidthInput">Certificate Width:</label>
      <input
        id="frameWidthInput"
        type="number"
        value={props.frameWidth}
        onChange={handleFrameWidthChange}
      />
      <br />
      <label htmlFor="frameHeightInput">Certificate Height:</label>
      <input
        id="frameHeightInput"
        type="number"
        value={props.frameHeight}
        onChange={handleFrameHeightChange}
      />
      <br />
      <input
        id="backgroundImageInput"
        type="file"
        accept="image/*"
        onChange={handleBackgroundImageChange}
      />
      <br />
      <button onClick={props.onSaveClick}>Save Design</button>
      <button onClick={props.onExport} style={{ marginTop: 10 }}>
        Export As Pdf
      </button>
      <div style={{ marginTop: 30 }}>Import From JSON</div>
      <textarea
        value={jsonText}
        onChange={handleJsonTextChange}
        style={{
          width: "100%",
          height: "100px",
          resize: "none",
          marginTop: 10,
        }}
      />
    </div>
  );
};

export default Sidebar;
