import { useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./App.css";
import Sidebar from "./components/Sidebar";
import Frame from "./components/Frame";
import { Box } from "./components/Box";
import jsPDF from "jspdf";
import html2canvas from "html2canvas";

function App() {
  const [frameWidth, setFrameWidth] = useState<number>(500);
  const [frameHeight, setFrameHeight] = useState<number>(500);
  const [backgroundImage, setBackgroundImage] = useState<string>("");
  const [box, setBox] = useState<{
    left: number;
    top: number;
  }>({
    left: 20,
    top: 290,
  });

  const handleDownload = () => {
    const data = {
      frameWidth: `${frameWidth}`,
      frameHeight: `${frameHeight}`,
      backgroundImage: `${backgroundImage}`,
      box: `${JSON.stringify(box)}`,
    };
    const jsonData = JSON.stringify(data);
    const blob = new Blob([jsonData], { type: "application/json" });
    const url = URL.createObjectURL(blob);
    const link = document.createElement("a");
    link.download = "data.json";
    link.href = url;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
    URL.revokeObjectURL(url);
  };

  const downloadScreenshot = () => {
    const frameDiv = document.getElementById("abdo")!;
    html2canvas(frameDiv).then((canvas) => {
      const link = document.createElement("a");
      link.download = "screenshot.png";
      link.href = canvas.toDataURL("image/png");
      link.click();
    });
  };

  const handleJSONinsert = (json: string) => {
    const obj = JSON.parse(json);
    console.log(obj);
    setFrameWidth(obj.frameWidth);
    setFrameHeight(obj.frameHeight);
    setBackgroundImage(obj.backgroundImage);
    setBox(JSON.parse(obj.box));
  };

  return (
    <div className="App">
      <Sidebar
        frameHeight={frameHeight}
        frameWidth={frameWidth}
        onFrameHeightChange={(height) => setFrameHeight(height)}
        onFrameWidthChange={(width) => setFrameWidth(width)}
        onBackgroundImageChange={(image) => {
          setBackgroundImage(image);
          console.log(image);
        }}
        box={box}
        onSaveClick={handleDownload}
        onJsonInsert={handleJSONinsert}
        onExport={downloadScreenshot}
      />

      <Frame
        frameHeight={frameHeight}
        frameWidth={frameWidth}
        backgroundImage={backgroundImage}
        onItemDrop={(item, left, top) => setBox({ top: top, left: left })}
        box={box}
      />
    </div>
  );
}

export default App;
